package org.ws4d.smartsensor.device;

import org.ws4d.java.communication.DPWSCommunicationManager;
import org.ws4d.java.service.DefaultService;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.URI;

public class SmartSensorService extends DefaultService {
	// global settings
    
	public static final String    NAMESPACE    		 	= "http://www.demo.com/bbsr";
	public static final String    PORTTYPE_BRIGHT    	= "Brightness";
	public static final String    ENDPOINT_PATH		 	= "SmartSensorService";
	public static final QName	  QN_PORTTYPE_BRIGHT	= new QName(PORTTYPE_BRIGHT, NAMESPACE);
	
	public final static URI DOCU_EXAMPLE_SERVICE_ID = new URI(ENDPOINT_PATH);
	
	public SmartSensorService() {
	       super(DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);

	       this.setServiceId(DOCU_EXAMPLE_SERVICE_ID); 
	       
	       // -- getValuesAction --
	       GetLightOperation getLightAct = new GetLightOperation();
	       addOperation(getLightAct);      
		}
	}