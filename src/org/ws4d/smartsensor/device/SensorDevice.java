package org.ws4d.smartsensor.device;


import org.ws4d.java.communication.DPWSCommunicationManager;
import org.ws4d.java.service.DefaultDevice;
import org.ws4d.java.types.LocalizedString;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameSet;


public class SensorDevice extends DefaultDevice{
	
	public static final String   NAMESPACE      = "http://www.demo.com/bbsr";
	public static final String   PORTTYPE       = "Sensor";
	public static final String   LOCALE_EN      = "en_GB";
	
	public SensorDevice() {
		
		super(DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);

		// set PortType
		this.setPortTypes(new QNameSet ( new QName (PORTTYPE, NAMESPACE )));

		// add device name ( name is language specific )
		this.addFriendlyName ("en -US", "SensorDevice");
		this.addFriendlyName ( LocalizedString.LANGUAGE_DE , "Sensor");

		// add device manufacturer ( manufacturer is language specific )
		this.addManufacturer ( LocalizedString.LANGUAGE_EN , "Test Inc.");
		this.addManufacturer ("de -DE", " Test GmbH ");

		this.addModelName ( LocalizedString.LANGUAGE_EN , "Sensor Model 1");
	}	
}

