/**********************************************************************************
 * Copyright (c) 2007 MATERNA Information & Communications and
 *   TU Dortmund, Dpt. of Computer Science, Chair 4, Distributed Systems
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 **********************************************************************************/

package org.ws4d.smartsensor.device;

import java.util.Random;

import org.ws4d.java.communication.CommunicationException;
import org.ws4d.java.schema.Element;
import org.ws4d.java.schema.SchemaUtil;
import org.ws4d.java.security.CredentialInfo;
import org.ws4d.java.service.InvocationException;
import org.ws4d.java.service.Operation;
import org.ws4d.java.service.parameter.ParameterValue;
import org.ws4d.java.service.parameter.ParameterValueManagement;

/**
 * The Temperature Action returns the current Temperature.
 * 
 * 
 */
public class GetLightOperation extends Operation
{	
	public static final String ACT_HW_NAME     = "LightOperation";
	public static final String PARAM_HW_OUTPUT = "light";
	
	
	public GetLightOperation () {
		super(ACT_HW_NAME, SmartSensorService.QN_PORTTYPE_BRIGHT);
		
		Element lightOutput = new Element(PARAM_HW_OUTPUT, SensorDevice.NAMESPACE, SchemaUtil.TYPE_INT);
		
		setOutput(lightOutput);
	}
	

	@Override
	protected ParameterValue invokeImpl(ParameterValue parameterValue, CredentialInfo credentialInfo) throws InvocationException, CommunicationException {

		Random rand = new Random();
		int MIN = 0;
		int MAX = 200;

		ParameterValue luminescenceOutput = createOutputValue();
	    
		ParameterValueManagement.setString(luminescenceOutput, PARAM_HW_OUTPUT, Integer.toString(rand.nextInt((MAX - MIN) + 1) + MIN));

        return luminescenceOutput;
	}
}

