package org.ws4d.smartsensor.device;

import java.io.IOException;

import org.ws4d.java.CoreFramework;

public class StartSensor {

	public static void main(String[] args) {

		
		CoreFramework.start(args);
		
		// create a device
		SensorDevice device =     new SensorDevice();
		
        // create a service
		SmartSensorService service = new SmartSensorService();
		
		// attach service to device
	    device.addService(service);
	    
	    // start device
	    try {
			device.start();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return;
	}
	
}
